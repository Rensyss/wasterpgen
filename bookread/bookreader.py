import requests
import docx2txt
# import docx

# from fake_useragent import UserAgent # - подмена юзер агента в случае проблем (подумать почему не ставится)
# from bs4 import BeautifulSoup
# import pandas as pd

# URL = 'http://docs.psta.ru/m', 'https://gitlab.com/volodink/rpgen/-/issues/1', 'https://mail.ru/'
urls = ()
ERRORS = {200: 'Открытый доступ',
          403: 'Доступ запрещён',
          404: 'Сайт не найден',
          429: 'Слишком много запросов',
          500: 'Ошибка сервера',
          503: 'Сервис недоступен',
          }

HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
                  ' Chrome/93.0.4577.82 YaBrowser/21.9.0.1044 Yowser/2.5 Safari/537.36',
    'accept': '*/*'}  # HEAD, нужная тема кста


URL = docx2txt.process("testz.docx").split() # Получение URL из docx файла


def get_html(url, params=None): #запрос на страницу
    rec = requests.get(url, headers=HEADERS, params=params)
    return rec


def access(): # Проверка доступа страницы
    for i in range(len(URL)):
        print(URL[i], ERRORS[get_html(URL[i]).status_code], get_html(
            URL[i]).status_code)  # Проверка доступа к сайту, .status_code убирается для получения развёрнутой ошибки


access()
