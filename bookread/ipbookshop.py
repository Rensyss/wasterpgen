import requests
from bs4 import BeautifulSoup
import time

ERRORS = {200: 'Открытый доступ',
          403: 'Доступ запрещён',
          404: 'Сайт не найден',
          429: 'Слишком много запросов',
          500: 'Ошибка сервера',
          502: 'Неправильный шлюз',
          503: 'Ссылка недоступна',
          }

URL = ['https://www.iprbookshop.ru/96845.html','https://gitlab.com/volodink/rpgen/-/issues/1', 'https://www.iprbookshop.ru/3138.html' 'https://mail.ru/', 'https://e.lanbook.com/book/155435',
       'https://e.lanbook.com/book/155435', 'https://e.lanbook.com/book/155435', 'https://e.lanbook.com/book/155435',
       'https://www.iprbookshop.ru/3138.html', 'https://urait.ru/events/2224', 'https://urait.ru/book/biologiya-testy-468234', 'https://urait.ru/book/programmirovanie-matematicheskaya-logika-475717']

HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
                  ' Chrome/93.0.4577.82 YaBrowser/21.9.0.1044 Yowser/2.5 Safari/537.36',
    'accept': '*/*'}  # HEAD, нужная тема кста


def get_html(url, params=None):  # запрос на страницу
    rec = requests.get(url, headers=HEADERS, params=params)
    return rec



def get_content_ipbookshop(html, retry=5):
    soup = BeautifulSoup(html, 'html.parser')
    try:
        items = soup.find('a', class_="btn btn-block btn-success").get_text().strip()
        if items == 'Читать':
            return True
    except Exception as ex:
        time.sleep(1)
        if retry:
            print(f'Ошибка ссылки {URL[i]}, осталось попыток: {retry}')
            return get_content_ipbookshop(html, retry=(retry - 1))
        else:
            return False, ERRORS[get_html(URL[i]).status_code], get_html(URL[i]).status_code


for i in range(len(URL)):
    html = get_html(URL[i])
    # print(get_content_urait(html.text), ' -', URL[i])
    print(get_content_ipbookshop(html.text))
